<?php

// If we're running from phar load the phar autoload file.
use Robo\Runner;
use Symfony\Component\Console\Output\ConsoleOutput;
use Consolidation\AnnotatedCommand\CommandFileDiscovery;

$pharPath = \Phar::running(true);
if ($pharPath) {
    $autoloaderPath = "$pharPath/vendor/autoload.php";
} else {
    if (file_exists(__DIR__.'/vendor/autoload.php')) {
        $autoloaderPath = __DIR__.'/vendor/autoload.php';
    } elseif (file_exists(__DIR__.'/../../autoload.php')) {
        $autoloaderPath = __DIR__ . '/../../autoload.php';
    } else {
        die("Could not find autoloader. Run 'composer install'.");
    }
}
$classLoader = require $autoloaderPath;

// Customization variables
$appName = "Release Manager";
$appVersion = trim(file_get_contents(__DIR__ . '/VERSION'));

// Gather command classes.
$discovery = new CommandFileDiscovery();
$discovery->setSearchPattern('*Command.php');
$commandClasses = $discovery->discover(__DIR__ . '/src/Commands', '\ReleaseManager\Commands');
$selfUpdateRepository = 'lpeabody/release-manager';
$configurationFilename = 'config/management.yml';

// Define our Runner, and pass it the command classes we provide.
$runner = new Runner($commandClasses);
$runner
    ->setSelfUpdateRepository($selfUpdateRepository)
    ->setConfigurationFilename($configurationFilename)
    ->setClassLoader($classLoader);

// Execute the command and return the result.
$output = new ConsoleOutput();
$statusCode = $runner->execute($argv, $appName, $appVersion, $output);
exit($statusCode);

<?php

namespace ReleaseManager\Tests;

use Symfony\Component\Process\Process;

trait CommandRunnerTrait {

    protected $testDir;
    protected $binPath = '/var/www/release';

    protected function runCommand($command, $bin_path = NULL) {
        if (is_null($bin_path)) {
            $bin_path = $this->binPath;
        }
        $build = [$bin_path, $command];
        $built_command = implode(' ', $build);
        $process = Process::fromShellCommandline($built_command, $this->testDir);
        $exit_status = $process->run();
        if (0 !== $exit_status) {
            throw new \Exception(sprintf("Non-zero exit code (%d) returned for command (%s) in directory (%s).", $exit_status, $built_command, $this->testDir));
        }
        return [
            'output' => $process->getOutput(),
            'exit_status' => $exit_status,
        ];
    }

    protected function executeSaveState() {
        return $this->runCommand('git:state:save');
    }

    protected function executeRestoreState() {
        return $this->runCommand('git:state:restore');
    }

    protected function executeStatus() {
        return $this->runCommand('git:status');
    }

    protected function resetGitTestDirectory() {
        $this->runCommand('git fetch && git clean -xf && git reset --hard && git stash clear', '');
    }

    protected function createUntrackedFile() {
        file_put_contents($this->testDir . '/untracked_file.txt', "I am untracked.\n");
    }

    protected function createIgnoredFile() {
        file_put_contents($this->testDir . '/ignored_file.txt', "I am ignored.\n");
    }

    protected function modifyExistingFile() {
        file_put_contents($this->testDir . '/commited_file.txt', "\nI am modified.\n", FILE_APPEND);
    }

}

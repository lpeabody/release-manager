<?php

namespace ReleaseManager\Tests;

use ReleaseManager\Commands\GitCommand;
use Robo\Robo;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\Output;
use Symfony\Component\Console\Tester\CommandTester;

trait CommandTesterTrait
{
    /** @var string */
    protected $appName;

    /** @var string */
    protected $appVersion;

    /**
     * Instantiate a new runner
     */
    public function setupCommandTester($appName, $appVersion)
    {
        // Define our invariants for our test
        $this->appName = $appName;
        $this->appVersion = $appVersion;
    }

    /**
     * Prepare our $argv array; put the app name in $argv[0] followed by
     * the command name and all command arguments and options.
     *
     * @param array $functionParameters should usually be func_get_args()
     * @param int $leadingParameterCount the number of function parameters
     *   that are NOT part of argv. Default is 2 (expected content and
     *   expected status code).
     */
    protected function argv($functionParameters, $leadingParameterCount = 2)
    {
        $argv = $functionParameters;
        $argv = array_slice($argv, $leadingParameterCount);
        array_unshift($argv, $this->appName);

        return $argv;
    }

    /**
     * Simulated front controller
     */
    protected function execute($argv, $commandClasses, $configurationFile = false)
    {
        // Define a global output object to capture the test results
        $output = new BufferedOutput();
        $output->setVerbosity(Output::VERBOSITY_VERY_VERBOSE);

        // We can only call `Runner::execute()` once; then we need to tear down.
        $runner = new \Robo\Runner($commandClasses);
        if ($configurationFile) {
            $runner->setConfigurationFilename($configurationFile);
        }
        $statusCode = $runner->execute($argv, $this->appName, $this->appVersion, $output);

        // Destroy our container so that we can call $runner->execute() again for the next test.
        \Robo\Robo::unsetContainer();

        // Return the output and status code.
        return [trim($output->fetch()), $statusCode];
    }

    /**
     * Executes a command with interactive inputs.
     *
     * @param string $command
     * @param array $inputs
     * @param array $command_extra
     *
     * @return array
     *   Returns the output of the command along with the status code.
     */
    protected function executeCommand($command_string, $inputs, $command_extra = []) {
        $argv = [$this->appName, $command_string];
        $tester = new \ReleaseManager\Tester\CommandTester();
        $runner = new \Robo\Runner($this->appName);
        $tester->setInput(array_merge(['command' => $command_string], $command_extra));
        $command = $runner->getCommandForTest($argv, GitCommand::class, $this->appName, $this->appVersion, $tester->getOutput());
        $tester->setCommand($command);
        $tester->setInputs($inputs);
        $status_code = $tester->execute();
        \Robo\Robo::unsetContainer();
        return [trim($tester->getDisplay()), $status_code];
    }
}

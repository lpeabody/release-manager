<?php

namespace ReleaseManager\Tests\Functional;

use PHPUnit\Framework\TestCase;
use ReleaseManager\Commands\GitCommand;
use ReleaseManager\Tests\CommandRunnerTrait;
use ReleaseManager\Tests\CommandTesterTrait;

final class StateTest extends TestCase {

    use CommandRunnerTrait;
    use CommandTesterTrait;

    protected $commandClasses;
    protected $initialStatus;
    protected $originalDir;
    protected const TEST_DIR = '/var/www/test_git_repos/test-02-save-state';
    protected $filesModifiedStatus = <<<MOD
On branch master
Your branch is up to date with 'origin/master'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   commited_file.txt

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	untracked_file.txt

no changes added to commit (use "git add" and/or "git commit -a")
MOD;
    protected $filesNotModifiedStatus = <<<MOD
On branch master
Your branch is up to date with 'origin/master'.

nothing to commit, working tree clean
MOD;

    /**
     * {@inheritdoc}
     */
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        $this->testDir = self::TEST_DIR;
        $this->originalDir = getcwd();
        parent::__construct($name, $data, $dataName);
    }

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        chdir($this->testDir);
        $this->setupCommandTester('Release Manager', '0.0.1-alpha1');
        $this->commandClasses = [GitCommand::class];
        $result = $this->executeStatus();
        $this->initialStatus = $result['output'];
        $this->createUntrackedFile();
        $this->modifyExistingFile();
        $this->createIgnoredFile();
        parent::setUp();
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        $this->resetGitTestDirectory();
        chdir($this->originalDir);
        parent::tearDown();
    }

    /**
     * Tests git:status.
     */
    public function testStatus() {
        $argv = ['Release Manager', 'git:status'];
        list($actualOutput, $statusCode) = $this->execute($argv, $this->commandClasses);
        $this->assertStringContainsString($this->filesModifiedStatus, $actualOutput);
        $this->assertEquals(0, $statusCode);
    }

    /**
     * Tests git:state:save.
     */
    public function testStateSave() {
        $argv = ['Release Manager', 'git:state:save'];
        list (,$statusCode)  = $this->execute($argv, $this->commandClasses);
        $this->assertEquals(0, $statusCode);
        $argv = ['Release Manager', 'git:status'];
        list($actualOutput, $statusCode) = $this->execute($argv, $this->commandClasses);
        $this->assertStringContainsString($this->filesNotModifiedStatus, $actualOutput);
        $this->assertEquals(0, $statusCode);
    }

    /**
     * Tests git:state:restore.
     */
    public function testStateRestore() {
        $argv = ['Release Manager', 'git:state:save'];
        list(, $statusCode) = $this->execute($argv, $this->commandClasses);
        $this->assertEquals(0, $statusCode);
        $argv = ['Release Manager', 'git:status'];
        list($statusOutput, $statusCode) = $this->execute($argv, $this->commandClasses);
        $this->assertStringContainsString($this->filesNotModifiedStatus, $statusOutput);
        $argv = ['Release Manager', 'git:state:restore'];
        list(, $statusCode) = $this->execute($argv, $this->commandClasses);
        $this->assertEquals(0, $statusCode);
        $argv = ['Release Manager', 'git:status'];
        list($statusOutput, $statusCode) = $this->execute($argv, $this->commandClasses);
        $this->assertStringContainsString($this->filesModifiedStatus, $statusOutput);
        $this->assertEquals(0, $statusCode);
    }
}

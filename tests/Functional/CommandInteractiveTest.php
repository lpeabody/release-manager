<?php

namespace ReleaseManager\Test\Functional;

use PHPUnit\Framework\TestCase;
use ReleaseManager\Tests\CommandTesterTrait;
use Symfony\Component\Console\Command\Command;
use ReleaseManager\Tester\CommandTester;

class CommandInteractiveTest extends TestCase {

    use CommandTesterTrait;

    public function testThis() {
        $this->setupCommandTester('Release Manager', '1.0.0-alpha1');
        /** @var Command $command */
        list($output, $status_code) = $this->executeCommand('git:interactive', [], ['answer' => 42]);
        $this->assertEquals(0, $status_code);
        $this->assertStringContainsString('The answer is 42.', $output);
    }
}

<?php

namespace ReleaseManager;

use Robo\Tasks;
use Symfony\Component\Console\Question\Question;

abstract class BaseTask extends Tasks {
    protected function askAutocomplete($question_text, $autocompletes, $default = NULL) {
        $helper = $this->getDialog();
        $question = new Question($this->formatQuestion($question_text));
        $question->setAutocompleterValues($autocompletes);
        return $helper->ask($this->input(), $this->output(), $question);
    }
}

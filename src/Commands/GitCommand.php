<?php

namespace ReleaseManager\Commands;

use Consolidation\AnnotatedCommand\AnnotationData;
use Consolidation\Log\Logger;
use ReleaseManager\BaseTask;
use Robo\Contract\VerbosityThresholdInterface;
use Robo\Robo;
use Robo\Symfony\SymfonyStyleInjector;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class GitCommand extends BaseTask {

    protected $stashes = [];

    /**
     * @command git:branches
     */
    public function gitBranches() {
        $result = $this->taskExec('git for-each-ref')
            ->silent(true)
            ->run();

        $output = $result->getMessage();
        $refs = explode("\n", $output);
        $branches = [];
        foreach ($refs as $ref) {
            $parts = preg_split('/\s+/', $ref);
            $branches[] = str_replace('refs/heads/', '', $parts[2]);
        }
        $rc_branch_name = $this->ask("What branch name to use for rc? ");
        $merge_branch = '';
        $merge_branches = [];
        $original_branch_list = $branches;
        do {
            if (!empty($merge_branch)) {
                $merge_branches[] = $merge_branch;
                $branches = array_diff($branches, $merge_branches);
            }
            $this->say(implode("\n", $branches) . "\n\n");

        } while ($merge_branch = $this->askAutocomplete("What branch to merge? ", $branches));

        $git_tasks = $this->taskExecStack()
            ->completionCode()
            ->exec('git checkout master')
            ->exec('git checkout -b ' . $rc_branch_name);

        foreach ($merge_branches as $merge_branch) {
            $git_tasks->exec('git merge --no-edit --no-ff ' . $merge_branch);
        }
        $git_tasks->run();
    }

    /**
     * @command git:interactive
     */
    public function gitInteractive($answer) {
        $output = $this->output();
        $this->io()->text("The answer is $answer.");
    }

    /**
     * @hook interact git:interactive
     */
    public function gitInteractiveInteract(InputInterface $input, OutputInterface $output, AnnotationData $annotationData) {
        if (!$input->getArgument('answer')) {
            $io = new SymfonyStyle($input, $output);
            $answer = $io->ask("what is the answer? ");
            $input->setArgument('answer', $answer);
        }
    }

    /**
     * @command git:state:save
     */
    public function gitStateSave() {
        $this->saveState();
    }

    /**
     * @command git:state:restore
     */
    public function gitStateRestore() {
        $this->restoreState();
    }

    /**
     * @command git:status
     */
    public function gitStatus() {
        $stack = $this->taskExecStack();
        $result = $stack
            ->printOutput(false)
            ->exec('git status')
            ->run();
        $message = $result->getMessage();
        $style = new SymfonyStyle($this->input(), $this->output());
        $style->text($message);
    }

    protected function clearScreen() {
        $this->output()->write(sprintf("\033\143"));
    }

    protected function saveState($include_untracked = TRUE) {
        $command = ['git', 'stash', 'push'];
        if ($include_untracked) {
            $command[] = '--include-untracked';
        }
        $stack = $this->taskExecStack();
        $result = $stack
            ->printOutput(false)
            ->exec(implode(' ', $command))
            ->run();
        $message = $result->getMessage();
        $style = new SymfonyStyle($this->input(), $this->output());
        $style->text($message);
    }

    protected function restoreState($index = TRUE) {
        $command = ['git', 'stash', 'pop'];
        if ($index) {
            $command[] = '--index';
        }
        $stack = $this->taskExecStack();
        $result = $stack
            ->printOutput(false)
            ->exec(implode(' ', $command))
            ->run();
        $message = $result->getMessage();
        $style = new SymfonyStyle($this->input(), $this->output());
        $style->text($message);
    }

}
